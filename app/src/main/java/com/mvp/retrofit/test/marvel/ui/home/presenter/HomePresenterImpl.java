package com.mvp.retrofit.test.marvel.ui.home.presenter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import com.mvp.retrofit.test.marvel.ui.home.interactor.HomeInteractorImpl;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomeInteractor;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomePresenter;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomeView;

public class HomePresenterImpl implements HomePresenter {

    private Context context;
    private HomeView view;
    private HomeInteractor interactor;
    private RecyclerView recyclerView;

    public HomePresenterImpl(Context context, HomeView view, RecyclerView recyclerView) {
        this.context = context;
        this.view = view;
        this.recyclerView = recyclerView;
        interactor = new HomeInteractorImpl();
    }


    @Override
    public void initDataRecycler() {
        if (view != null)
            interactor.initHomeOptions(context, view, recyclerView);
    }
}
