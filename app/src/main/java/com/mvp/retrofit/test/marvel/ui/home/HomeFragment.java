package com.mvp.retrofit.test.marvel.ui.home;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.mvp.retrofit.test.marvel.R;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomePresenter;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomeView;
import com.mvp.retrofit.test.marvel.ui.home.presenter.HomePresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeFragment extends Fragment implements HomeView {

    @BindView(R.id.recycler_menu_main)
    RecyclerView recyclerMenuMain;

    private ProgressDialog progress;
    private HomePresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_home, container, false);
        ButterKnife.bind(this, view);

        progress = new ProgressDialog(getContext());

        presenter = new HomePresenterImpl(getContext(), this, recyclerMenuMain);
        presenter.initDataRecycler();

        return view;
    }

    @Override
    public void showDialog() {
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Cargando información...");
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public void dismissDialog() {
        progress.dismiss();
    }

}
