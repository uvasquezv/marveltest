package com.mvp.retrofit.test.marvel.ui.home.interfaces;

public interface HomeView {

    void showDialog();
    void dismissDialog();
}
