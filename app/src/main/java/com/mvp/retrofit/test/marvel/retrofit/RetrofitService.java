package com.mvp.retrofit.test.marvel.retrofit;

import com.mvp.retrofit.test.marvel.retrofit.model.SuperHeroes;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitService {

    @GET("bins/bvyob")
    Call<SuperHeroes> obtenerListaSuperheroes();
}
