package com.mvp.retrofit.test.marvel.retrofit.model;

import java.io.Serializable;

public class Heroe implements Serializable {

    private String name;
    private String photo;
    private String realName;
    private String height;
    private String power;
    private String abilities;
    private String groups;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getAbilities() {
        return abilities;
    }

    public void setAbilities(String abilities) {
        this.abilities = abilities;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Heroe{" +
                "name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", realName='" + realName + '\'' +
                ", height='" + height + '\'' +
                ", power='" + power + '\'' +
                ", abilities='" + abilities + '\'' +
                ", groups='" + groups + '\'' +
                '}';
    }
}
