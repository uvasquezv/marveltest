package com.mvp.retrofit.test.marvel.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.mvp.retrofit.test.marvel.R;
import com.mvp.retrofit.test.marvel.ui.home.HomeFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    private static Toolbar toolbar;

    private static FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        fragmentManager = getSupportFragmentManager();

        toolbar = findViewById(R.id.toolbar);

        showFragment(new HomeFragment(), "Marvel");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (toolbar.getTitle().equals("Details"))
            showFragment(new HomeFragment(), "Home");
        else
            finish();
    }

    public static void showFragment(Fragment fragment, String title) {
        initToolbar(title);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainContainer, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null).commit();
    }

    private static void initToolbar(String title) {
        toolbar.setTitle(title);
        if (title.equals("Details")) {
            toolbar.setNavigationIcon(R.drawable.ic_back);
            toolbar.setNavigationOnClickListener(v -> showFragment(new HomeFragment(), "Home"));
        } else
            toolbar.setNavigationIcon(null);

    }

}
