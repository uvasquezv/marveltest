package com.mvp.retrofit.test.marvel.ui.home.interactor;

import android.content.Context;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mvp.retrofit.test.marvel.R;
import com.mvp.retrofit.test.marvel.retrofit.RetrofitService;
import com.mvp.retrofit.test.marvel.retrofit.model.SuperHeroes;
import com.mvp.retrofit.test.marvel.ui.home.adapter.AdapterHome;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomeInteractor;
import com.mvp.retrofit.test.marvel.ui.home.interfaces.HomeView;
import com.mvp.retrofit.test.marvel.utils.GridSpacingItemDecoration;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeInteractorImpl implements HomeInteractor {

    private Retrofit retrofit;

    @Override
    public void initHomeOptions(Context context, HomeView view, RecyclerView recyclerView) {
        view.showDialog();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, 15, true));


        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitService service = retrofit.create(RetrofitService.class);

        Call<SuperHeroes> response = service.obtenerListaSuperheroes();

        response.enqueue(new Callback<SuperHeroes>() {
            @Override
            public void onResponse(Call<SuperHeroes> call, Response<SuperHeroes> response) {
                if (response.isSuccessful()) {
                    SuperHeroes heroes = response.body();

                    recyclerView.setAdapter(new AdapterHome(context, heroes.getSuperheroes()));

                    view.dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<SuperHeroes> call, Throwable t) {
                view.dismissDialog();
                Toast.makeText(context, R.string.not_connections, Toast.LENGTH_LONG).show();

            }
        });
    }
}
