package com.mvp.retrofit.test.marvel.ui.home.holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mvp.retrofit.test.marvel.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderHome extends RecyclerView.ViewHolder {

    @BindView(R.id.cardView)
    public CardView cardView;
    @BindView(R.id.circleImage)
    public CircleImageView imageView;
    @BindView(R.id.nameHeroe)
    public TextView nameHeroe;

    public ViewHolderHome(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        cardView.setElevation(9.0f);
    }
}
