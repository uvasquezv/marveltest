package com.mvp.retrofit.test.marvel.retrofit.model;

import java.util.ArrayList;

public class SuperHeroes {

    private ArrayList<Heroe> superheroes;

    public ArrayList<Heroe> getSuperheroes() {
        return superheroes;
    }

    public void setSuperheroes(ArrayList<Heroe> superheroes) {
        this.superheroes = superheroes;
    }

    @Override
    public String toString() {
        return "SuperHeroes{" +
                "superheroes=" + superheroes +
                '}';
    }
}
