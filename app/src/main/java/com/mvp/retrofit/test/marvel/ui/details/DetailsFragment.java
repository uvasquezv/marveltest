package com.mvp.retrofit.test.marvel.ui.details;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mvp.retrofit.test.marvel.R;
import com.mvp.retrofit.test.marvel.retrofit.model.Heroe;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailsFragment extends Fragment {


    @BindView(R.id.heroImage)
    CircleImageView heroImage;
    @BindView(R.id.nameHero)
    TextView nameHero;
    @BindView(R.id.realName)
    TextView realName;
    @BindView(R.id.height)
    TextView height;
    @BindView(R.id.power)
    TextView power;
    @BindView(R.id.abilities)
    TextView abilities;
    @BindView(R.id.groups)
    TextView groups;

    private static final String HERO_OBJECT = "heroObject";

    private Heroe heroe;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(Heroe heroe) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(HERO_OBJECT, heroe);
        fragment.setArguments(args);
        return fragment;
    }

    //-------------------- override methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.heroe = (Heroe) getArguments().getSerializable(HERO_OBJECT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_details, container, false);

        ButterKnife.bind(this, view);

        initData();

        return view;
    }

    private void initData() {
        RequestOptions resquestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_photo_24px)
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(getContext())
                .asBitmap()
                .load(heroe.getPhoto())
                .apply(resquestOptions)
                .into(heroImage);

        nameHero.setText(heroe.getName());

        realName.setText(heroe.getRealName());

        height.setText(heroe.getHeight());

        power.setText(heroe.getPower());

        abilities.setText(heroe.getAbilities());

        groups.setText(heroe.getGroups());
    }

}
