package com.mvp.retrofit.test.marvel.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mvp.retrofit.test.marvel.R;
import com.mvp.retrofit.test.marvel.retrofit.model.Heroe;
import com.mvp.retrofit.test.marvel.ui.MainActivity;
import com.mvp.retrofit.test.marvel.ui.details.DetailsFragment;
import com.mvp.retrofit.test.marvel.ui.home.holder.ViewHolderHome;

import java.util.ArrayList;

public class AdapterHome extends RecyclerView.Adapter<ViewHolderHome> {

    Context context;
    ArrayList<Heroe> heroeList;

    public AdapterHome(Context context, ArrayList<Heroe> arrayList) {
        this.context = context;
        this.heroeList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolderHome onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_heroe, null, false);
        return new ViewHolderHome(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderHome holder, int position) {
        RequestOptions resquestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_photo_24px)
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(context)
                .asBitmap()
                .load(heroeList.get(position).getPhoto())
                .apply(resquestOptions)
                .into(holder.imageView);

        holder.nameHeroe.setText(heroeList.get(position).getName());

        holder.cardView.setOnClickListener(v -> MainActivity.showFragment(DetailsFragment.newInstance(heroeList.get(position)), "Details"));

    }

    @Override
    public int getItemCount() {
        return heroeList.size();
    }
}
