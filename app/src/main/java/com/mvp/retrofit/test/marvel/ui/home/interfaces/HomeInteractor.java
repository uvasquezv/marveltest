package com.mvp.retrofit.test.marvel.ui.home.interfaces;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

public interface HomeInteractor {

    void initHomeOptions(Context context, HomeView view, RecyclerView recyclerView);

}
